package main

import (
	"database/sql"
	"log"
	"time"

	ctrl "gitlab.com/orbitx/a3/controllers"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

func main() {
	db, err := sql.Open("mysql", "root:root@/orbitx_a3")
	if err != nil {
		log.Fatalf("Got error when connect database, the error is '%v'", err)
	}
	db.SetMaxOpenConns(25)
	db.SetMaxIdleConns(10)
	db.SetConnMaxLifetime(time.Minute)

	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Route => handler
	// auth
	e.POST("/signin", ctrl.Signin(db))
	e.POST("/register", ctrl.Register(db))
	e.POST("/register/quick", ctrl.QuickRegister(db))

	// servers
	e.POST("/servers", ctrl.CreateServer(db))
	e.GET("/servers", ctrl.GetServers(db))
	e.GET("/servers/:id", ctrl.GetServer(db))
	e.DELETE("/servers/:id", ctrl.RemoveServer(db))

	e.GET("/servers/:id/games", ctrl.GetGamesOfServer(db))
	e.POST("/servers/:id/games", ctrl.PostGameToServer(db))
	e.DELETE("/servers/:id/games", ctrl.DeleteGameFromServer(db))

	// developers
	e.POST("/developers", ctrl.CreateDeveloper(db))
	e.GET("/developers", ctrl.GetDevelopers(db))
	e.GET("/developers/:id", ctrl.GetDeveloper(db))
	e.PATCH("/developers/:id", ctrl.UpdateDeveloper(db))
	e.DELETE("/developers/:id", ctrl.RemoveDeveloper(db))

	e.GET("/developers/:id/games", ctrl.GetGamesOfDeveloper(db))
	e.GET("/developers/:id/servers", ctrl.GetServersOfDeveloper(db))

	// games
	e.POST("/games", ctrl.CreateGame(db))
	e.GET("/games", ctrl.GetGames(db))
	e.GET("/games/:id", ctrl.GetGame(db))
	e.PATCH("/games/:id", ctrl.UpdateGame(db))
	e.DELETE("/games/:id", ctrl.RemoveGame(db))
	e.GET("/games/:id/users", ctrl.GetUsersOfGame(db))
	e.GET("/games/:id/skills", ctrl.GetSkillsOfGame(db))
	e.GET("/games/:id/achivements", ctrl.GetAchivementsOfGame(db))

	// users
	e.GET("/users/:id", ctrl.GetUser(db))
	e.GET("/users/:id/games", ctrl.GetGamesOfUser(db))

	// Start server
	e.Logger.Fatal(e.Start(":8080"))
}
