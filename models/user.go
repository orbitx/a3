package models

type User struct {
	ID                  int     `json:"id"`
	Nickname            string  `json:"nickname"`
	Firstname           string  `json:"firstname"`
	Lastname            string  `json:"lastname"`
	Email               string  `json:"email"`
	Phone               string  `json:"phone"`
	ProgressMedian      float64 `json:"progressMedian"`
	ProgressDeviation   float64 `json:"progressDeviation"`
	Progress            int     `json:"progress"`
	Status              string  `json:"status"`
	StatusChangedAt     string  `json:"statusChangedAt"`
	StatusMessage       string  `json:"statusMessage"`
	CreatedAt           string  `json:"createdAt"`
	ModifiedAt          string  `json:"modifiedAt"`
	LatestTokenIssuedAt string  `json:"latestTokenIssueedAt"`
	Password            string
}
