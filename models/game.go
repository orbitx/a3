package models

type Game struct {
	ID                  int    `json:"id"`
	Title               string `json:"title"`
	DeveloperID         int    `json:"developerId"`
	SecretKey           string `json:"secretKey"`
	Status              string `json:"status"`
	StatusChangedAt     string `json:"statusChangedAt"`
	StatusMessage       string `json:"statusMessage"`
	CreatedAt           string `json:"createdAt"`
	ModifiedAt          string `json:"modifiedAt"`
	LatestTokenIssuedAt string `json:"latestTokenIssueedAt"`
}
