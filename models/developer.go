package models

type Developer struct {
	ID                  int    `json:"id"`
	Firstname           string `json:"firstname"`
	Lastname            string `json:"lastname"`
	Email               string `json:"email"`
	Phone               string `json:"phone"`
	Affiliation         string `json:"affiliation"`
	Status              string `json:"status"`
	StatusChangedAt     string `json:"statusChangedAt"`
	StatusMessage       string `json:"statusMessage"`
	CreatedAt           string `json:"createdAt"`
	ModifiedAt          string `json:"modifiedAt"`
	LatestTokenIssuedAt string `json:"latestTokenIssueedAt"`
	Password            string
}
