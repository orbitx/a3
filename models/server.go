package models

type Server struct {
	ID                  int    `json:"id"`
	DeveloperID         int    `json:"developerId"`
	SecretKey           string `json:"secretKey"`
	Status              string `json:"status"`
	StatusChangedAt     string `json:"statusChangedAt"`
	StatusMessage       string `json:"statusMessage"`
	CreatedAt           string `json:"createdAt"`
	ModifiedAt          string `json:"moifiedAt"`
	LatestTokenIssuedAt string `json:"latestTokenIssuedAt"`
}
