package controllers

import (
	"database/sql"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/orbitx/a3/models"
)

func GetUser(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		u := models.User{}
		uid := c.Param("id")
		err := db.QueryRow(
			`SELECT
				id, nickname, firstname, lastname, email, phone,
				progress_median, progress_deviation, progress,
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM users
			WHERE id = ?`, uid).Scan(
			&u.ID, &u.Nickname, &u.Firstname, &u.Lastname, &u.Email, &u.Phone,
			&u.ProgressMedian, &u.ProgressDeviation, &u.Progress,
			&u.LatestTokenIssuedAt,
			&u.Status, &u.StatusChangedAt, &u.StatusMessage,
			&u.CreatedAt, &u.ModifiedAt)
		if err != nil {
			if err == sql.ErrNoRows {
				return echo.NewHTTPError(http.StatusNotFound, "user not found")
			}
			return err
		}
		c.JSON(http.StatusOK, u)
		return nil
	}
}

func GetGamesOfUser(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		games := make([]models.Game, 0)
		uid := c.Param("id")
		orderBy := c.QueryParam("sort")
		offset := c.QueryParam("offset")
		limit := c.QueryParam("limit")

		if orderBy == "" {
			orderBy = "id"
		}

		if offset == "" {
			offset = "0"
		}

		if limit == "" || limit > "100" {
			limit = "10"
		}

		err := db.QueryRow(`SELECT id FROM users WHERE id = $1`, uid).Scan()
		if err != nil {
			return echo.NewHTTPError(http.StatusNotFound, "user not found")
		}

		rows, err := db.Query(
			`SELECT
				id, title, description, developer_id, server_id,
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM users_games
			JOIN games ON games.id = users_games.game_id
			WHERE user_id = ?
			ORDER BY ? LIMIT ? OFFSET ?`,
			uid, orderBy, limit, offset)
		if err != nil {
			return err
		}

		for rows.Next() {
			g := models.Game{}
			err = rows.Scan(
				&g.ID, &g.Title, &g.DeveloperID,
				&g.LatestTokenIssuedAt,
				&g.Status, &g.StatusChangedAt, &g.StatusMessage,
				&g.CreatedAt, &g.ModifiedAt)

			if err != nil {
				return err
			}

			games = append(games, g)
		}

		c.JSON(http.StatusOK, games)
		return nil
	}
}
