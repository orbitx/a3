package controllers

import (
	"database/sql"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/orbitx/a3/models"
	"gitlab.com/orbitx/a3/utils"
)

func CreateGame(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		g := new(models.Game)
		if err := c.Bind(g); err != nil {
			return err
		}
		if g.DeveloperID == 0 {
			return echo.NewHTTPError(http.StatusBadRequest, "Server ID or Developer ID not specified.")
		}
		g.SecretKey = utils.RandomString(64)

		err := db.QueryRow(
			"INSERT INTO games(title, developer_id, secret_key) VALUES($1, $2, $3, $4) RETURNING id, created_at",
			g.Title, g.DeveloperID, g.SecretKey).Scan(&g.ID, &g.CreatedAt)
		if err != nil {
			return err
		}

		c.JSON(http.StatusCreated, g)
		return nil
	}
}

func GetGame(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id := c.Param("id")
		g := new(models.Game)
		err := db.QueryRow(
			`SELECT
				id, title, description, developer_id,
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM games
			WHERE id = $1`, id).Scan(
			&g.ID, &g.Title, &g.DeveloperID,
			&g.LatestTokenIssuedAt,
			&g.Status, &g.StatusChangedAt, &g.StatusMessage,
			&g.CreatedAt, &g.ModifiedAt)
		if err != nil {
			if err == sql.ErrNoRows {
				return echo.NewHTTPError(404, "Game not found")
			}
			return err
		}
		c.JSON(http.StatusOK, g)
		return nil
	}
}

func GetGames(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		games := make([]models.Game, 0)
		orderBy := c.QueryParam("sort")
		offset := c.QueryParam("offset")
		limit := c.QueryParam("limit")

		if orderBy == "" {
			orderBy = "id"
		}

		if offset == "" {
			offset = "0"
		}

		if limit == "" || limit > "100" {
			limit = "10"
		}

		rows, err := db.Query(
			`SELECT
				id, title, description, developer_id,
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM games
			ORDER BY ? LIMIT ? OFFSET ?`, orderBy, offset, limit)
		if err != nil {
			return err
		}

		for rows.Next() {
			game := models.Game{}
			err = rows.Scan(
				&game.ID, &game.Title, &game.DeveloperID,
				&game.LatestTokenIssuedAt,
				&game.Status, &game.StatusChangedAt, &game.StatusMessage,
				&game.CreatedAt, &game.ModifiedAt)

			if err != nil {
				return err
			}

			games = append(games, game)
		}

		c.JSON(http.StatusOK, games)
		return nil
	}
}

func UpdateGame(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id := c.Param("id")
		g := new(models.Game)
		err := db.QueryRow(
			`SELECT
				id, title, description, developer_id,
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM games
			WHERE id = ?`, id).Scan(
			&g.ID, &g.Title, &g.DeveloperID,
			&g.LatestTokenIssuedAt,
			&g.Status, &g.StatusChangedAt, &g.StatusMessage,
			&g.CreatedAt, &g.ModifiedAt)
		if err != nil {
			if err == sql.ErrNoRows {
				return echo.NewHTTPError(404, "Game not found")
			}
			return err
		}

		if err = c.Bind(g); err != nil {
			return err
		}

		_, err = db.Exec(
			`UPDATE games
			SET
				title = ?,
				description = ?,
				developer_id = ?,
				latest_token_issued_at = ?,
				status = ?,
				status_changed_at = ?,
				status_message = ?,
				created_at = ?,
				modified_at = ?
			WHERE id = ?`,
			g.Title, g.DeveloperID,
			g.LatestTokenIssuedAt,
			g.Status, g.StatusChangedAt, g.StatusMessage,
			g.CreatedAt, g.ModifiedAt, g.ID)
		if err != nil {
			return err
		}

		c.JSON(http.StatusOK, g)
		return nil
	}
}

func RemoveGame(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id := c.Param("id")
		g := new(models.Game)
		err := db.QueryRow(
			`SELECT
				id, title, description, developer_id,
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM games
			WHERE id = ?`, id).Scan(
			&g.ID, &g.Title, &g.DeveloperID,
			&g.LatestTokenIssuedAt,
			&g.Status, &g.StatusChangedAt, &g.StatusMessage,
			&g.CreatedAt, &g.ModifiedAt)
		if err != nil {
			if err == sql.ErrNoRows {
				return echo.NewHTTPError(404, "Game not found")
			}
			return err
		}

		_, err = db.Exec(`DELETE FROM games WHERE id = ?`, g.ID)
		if err != nil {
			return err
		}

		c.JSON(http.StatusOK, g)
		return nil
	}
}

func GetUsersOfGame(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		users := make([]models.User, 0)
		gid := c.Param("id")
		orderBy := c.QueryParam("sort")
		offset := c.QueryParam("offset")
		limit := c.QueryParam("limit")

		if orderBy == "" {
			orderBy = "id"
		}

		if offset == "" {
			offset = "0"
		}

		if limit == "" || limit > "100" {
			limit = "10"
		}

		err := db.QueryRow(`SELECT id FROM games WHERE id = ?`, gid).Scan()
		if err != nil {
			return echo.NewHTTPError(http.StatusNotFound, "game not found")
		}

		rows, err := db.Query(
			`SELECT
				id, nickname, firstname, lastname, email, phone,
				progress_median, progress_deviation, progress,
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM users_games
			JOIN users ON users.id = users_games.user_id
			WHERE user_id = ?
			ORDER BY ? LIMIT ? OFFSET ?`,
			gid, orderBy, limit, offset)
		if err != nil {
			return err
		}

		for rows.Next() {
			u := models.User{}
			err = rows.Scan(
				&u.Nickname, &u.Firstname, &u.Lastname, &u.Email, &u.Phone,
				&u.ProgressMedian, &u.ProgressDeviation, &u.Progress,
				&u.LatestTokenIssuedAt,
				&u.Status, &u.StatusChangedAt, &u.StatusMessage,
				&u.CreatedAt, &u.ModifiedAt)

			if err != nil {
				return err
			}

			users = append(users, u)
		}

		c.JSON(http.StatusOK, users)
		return nil
	}
}

func GetSkillsOfGame(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		return nil
	}
}

func GetAchivementsOfGame(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		return nil
	}
}
