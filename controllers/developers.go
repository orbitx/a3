package controllers

import (
	"database/sql"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/orbitx/a3/models"
)

func CreateDeveloper(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		d := new(models.Developer)
		if err := c.Bind(d); err != nil {
			return err
		}

		err := db.QueryRow(
			`INSERT INTO developers(
				firstname, lastname, email, password, phone, affiliation
			) VALUES(?, ?, ?, ?, ?, ?) RETURNING id, created_at`,
			d.Firstname, d.Lastname, d.Email, d.Password, d.Phone).Scan(&d.ID, &d.CreatedAt)
		if err != nil {
			return err
		}

		c.JSON(http.StatusCreated, d)
		return nil
	}
}

func GetDeveloper(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		d := models.Developer{}
		did := c.Param("id")
		err := db.QueryRow(
			`SELECT
				id, firstname, lastname, email, phone,
				progress_median, progress_deviation, progress,
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM developers
			WHERE id = ?`, did).Scan(
			&d.ID, &d.Firstname, &d.Lastname, &d.Email, &d.Phone,
			&d.LatestTokenIssuedAt,
			&d.Status, &d.StatusChangedAt, &d.StatusMessage,
			&d.CreatedAt, &d.ModifiedAt)
		if err != nil {
			if err == sql.ErrNoRows {
				return echo.NewHTTPError(http.StatusNotFound, "developer not found")
			}
			return err
		}
		c.JSON(http.StatusOK, d)
		return nil
	}
}

func GetDevelopers(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		developers := make([]models.Developer, 0)
		orderBy := c.QueryParam("sort")
		offset := c.QueryParam("offset")
		limit := c.QueryParam("limit")

		if orderBy == "" {
			orderBy = "id"
		}

		if offset == "" {
			offset = "0"
		}

		if limit == "" || limit > "100" {
			limit = "10"
		}

		rows, err := db.Query(
			`SELECT
				id, developer_id, 
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM servers
			ORDER BY ? LIMIT ? OFFSET ?`,
			orderBy, limit, offset)
		if err != nil {
			return err
		}

		for rows.Next() {
			d := models.Developer{}
			err = rows.Scan(
				&d.ID, &d.Firstname, &d.Lastname, &d.Email, &d.Phone,
				&d.LatestTokenIssuedAt,
				&d.Status, &d.StatusChangedAt, &d.StatusMessage,
				&d.CreatedAt, &d.ModifiedAt)

			if err != nil {
				return err
			}

			developers = append(developers, d)
		}

		c.JSON(http.StatusOK, developers)
		return nil
	}
}

func UpdateDeveloper(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id := c.Param("id")
		d := new(models.Developer)
		err := db.QueryRow(
			`SELECT
				id, firstname, lastname, email, phone,
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM developers
			WHERE id = ?`, id).Scan(
			&d.ID, &d.Firstname, &d.Lastname, &d.Email, &d.Phone,
			&d.LatestTokenIssuedAt,
			&d.Status, &d.StatusChangedAt, &d.StatusMessage,
			&d.CreatedAt, &d.ModifiedAt)
		if err != nil {
			if err == sql.ErrNoRows {
				return echo.NewHTTPError(404, "Game not found")
			}
			return err
		}

		if err = c.Bind(d); err != nil {
			return err
		}

		_, err = db.Exec(
			`UPDATE developers
			SET
				firstname = ?,
				lastname = ?,
				email = ?,
				phone = ?,
				latest_token_issued_at = ?,
				status = ?,
				status_changed_at = ?,
				status_message = ?,
				created_at = ?,
				modified_at = ?
			WHERE id = ?`,
			&d.Firstname, &d.Lastname, &d.Email, &d.Phone,
			&d.LatestTokenIssuedAt,
			&d.Status, &d.StatusChangedAt, &d.StatusMessage,
			&d.CreatedAt, &d.ModifiedAt)
		if err != nil {
			return err
		}

		c.JSON(http.StatusOK, d)
		return nil
	}
}

func RemoveDeveloper(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		d := models.Developer{}
		did := c.Param("id")
		err := db.QueryRow(
			`SELECT
				id, firstname, lastname, email, phone,
				progress_median, progress_deviation, progress,
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM developers
			WHERE id = ?`, did).Scan(
			&d.ID, &d.Firstname, &d.Lastname, &d.Email, &d.Phone,
			&d.LatestTokenIssuedAt,
			&d.Status, &d.StatusChangedAt, &d.StatusMessage,
			&d.CreatedAt, &d.ModifiedAt)
		if err != nil {
			if err == sql.ErrNoRows {
				return echo.NewHTTPError(http.StatusNotFound, "developer not found")
			}
			return err
		}

		_, err = db.Exec(`DELETE FROM developers WHERE id = ?`, d.ID)
		if err != nil {
			return err
		}

		c.JSON(http.StatusOK, d)
		return nil
	}
}

func GetGamesOfDeveloper(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		games := make([]models.Game, 0)
		did := c.Param("id")
		orderBy := c.QueryParam("sort")
		offset := c.QueryParam("offset")
		limit := c.QueryParam("limit")

		if orderBy == "" {
			orderBy = "id"
		}

		if offset == "" {
			offset = "0"
		}

		if limit == "" || limit > "100" {
			limit = "10"
		}

		err := db.QueryRow(`SELECT id FROM developers WHERE id = ?`, did).Scan()
		if err != nil {
			return echo.NewHTTPError(http.StatusNotFound, "server not found")
		}

		rows, err := db.Query(
			`SELECT
				id, title, description, developer_id,
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM games
			WHERE developer_id = ?
			ORDER BY ? LIMIT ? OFFSET ?`,
			did, orderBy, limit, offset)
		if err != nil {
			return err
		}

		for rows.Next() {
			g := models.Game{}
			err = rows.Scan(
				&g.ID, &g.Title, &g.DeveloperID,
				&g.LatestTokenIssuedAt,
				&g.Status, &g.StatusChangedAt, &g.StatusMessage,
				&g.CreatedAt, &g.ModifiedAt)

			if err != nil {
				return err
			}

			games = append(games, g)
		}

		c.JSON(http.StatusOK, games)
		return nil
	}
}

func GetServersOfDeveloper(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		servers := make([]models.Server, 0)
		did := c.Param("id")
		orderBy := c.QueryParam("sort")
		offset := c.QueryParam("offset")
		limit := c.QueryParam("limit")

		if orderBy == "" {
			orderBy = "id"
		}

		if offset == "" {
			offset = "0"
		}

		if limit == "" || limit > "100" {
			limit = "10"
		}

		err := db.QueryRow(`SELECT id FROM developers WHERE id = ?`, did).Scan()
		if err != nil {
			return echo.NewHTTPError(http.StatusNotFound, "server not found")
		}

		rows, err := db.Query(
			`SELECT
				id, developer_id, 
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM servers
			WHERE developer_id = ?
			ORDER BY ? LIMIT ? OFFSET ?`,
			did, orderBy, limit, offset)
		if err != nil {
			return err
		}

		for rows.Next() {
			s := models.Server{}
			err = rows.Scan(
				&s.ID, &s.DeveloperID,
				&s.LatestTokenIssuedAt,
				&s.Status, &s.StatusChangedAt, &s.StatusMessage,
				&s.CreatedAt, &s.ModifiedAt)

			if err != nil {
				return err
			}

			servers = append(servers, s)
		}

		c.JSON(http.StatusOK, servers)
		return nil
	}
}
