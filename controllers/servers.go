package controllers

import (
	"database/sql"
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/orbitx/a3/models"
	"gitlab.com/orbitx/a3/utils"
)

func CreateServer(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		s := new(models.Server)
		if err := c.Bind(s); err != nil {
			return err
		}
		if s.DeveloperID == 0 {
			return echo.NewHTTPError(http.StatusBadRequest, "Developer ID not specified.")
		}
		s.SecretKey = utils.RandomString(64)

		err := db.QueryRow(
			`INSERT INTO servers(developer_id, secret_key) VALUES(?, ?, ?, ?) RETURNING id, created_at`,
			s.DeveloperID, s.SecretKey).Scan(&s.ID, &s.CreatedAt)
		if err != nil {
			return err
		}

		c.JSON(http.StatusCreated, s)
		return nil
	}
}

func GetServer(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id := c.Param("id")
		s := new(models.Server)
		err := db.QueryRow(
			`SELECT
				id, developer_id, 
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM servers
			WHERE id = ?`, id).Scan(
			&s.ID, &s.DeveloperID,
			&s.LatestTokenIssuedAt,
			&s.Status, &s.StatusChangedAt, &s.StatusMessage,
			&s.CreatedAt, &s.ModifiedAt)
		if err != nil {
			if err == sql.ErrNoRows {
				return echo.NewHTTPError(404, "Server not found")
			}
			return err
		}
		c.JSON(http.StatusOK, s)
		return nil
	}
}

func GetServers(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		servers := make([]models.Server, 0)
		orderBy := c.QueryParam("sort")
		offset := c.QueryParam("offset")
		limit := c.QueryParam("limit")

		if orderBy == "" {
			orderBy = "id"
		}

		if offset == "" {
			offset = "0"
		}

		if limit == "" || limit > "100" {
			limit = "10"
		}

		rows, err := db.Query(
			`SELECT
				id, developer_id, 
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM servers
			ORDER BY ? LIMIT ? OFFSET ?`,
			orderBy, limit, offset)
		if err != nil {
			return err
		}

		for rows.Next() {
			s := models.Server{}
			err = rows.Scan(
				&s.ID, &s.DeveloperID,
				&s.LatestTokenIssuedAt,
				&s.Status, &s.StatusChangedAt, &s.StatusMessage,
				&s.CreatedAt, &s.ModifiedAt)

			if err != nil {
				return err
			}

			servers = append(servers, s)
		}

		c.JSON(http.StatusOK, servers)
		return nil
	}
}

func UpdateServer(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id := c.Param("id")
		s := new(models.Server)
		err := db.QueryRow(
			`SELECT
				id, developer_id, 
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM servers
			WHERE id = ?`, id).Scan(
			&s.ID, &s.DeveloperID,
			&s.LatestTokenIssuedAt,
			&s.Status, &s.StatusChangedAt, &s.StatusMessage,
			&s.CreatedAt, &s.ModifiedAt)
		if err != nil {
			if err == sql.ErrNoRows {
				return echo.NewHTTPError(404, "Server not found")
			}
			return err
		}

		if err = c.Bind(s); err != nil {
			return err
		}

		_, err = db.Exec(
			`UPDATE servers
			SET
				developer_id = ?,
				latest_token_issued_at = ?,
				status = ?,
				status_changed_at = ?,
				status_message = ?,
				created_at = ?,
				modified_at = ?
			WHERE id = ?`,
			s.DeveloperID,
			s.LatestTokenIssuedAt,
			s.Status, s.StatusChangedAt, s.StatusMessage,
			s.CreatedAt, s.ModifiedAt,
			s.ID)
		if err != nil {
			return err
		}

		c.JSON(http.StatusOK, s)
		return nil
	}
}

func RemoveServer(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id := c.Param("id")
		s := new(models.Server)
		err := db.QueryRow(
			`SELECT
				id, developer_id, 
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM servers
			WHERE id = ?`, id).Scan(
			&s.ID, &s.DeveloperID,
			&s.LatestTokenIssuedAt,
			&s.Status, &s.StatusChangedAt, &s.StatusMessage,
			&s.CreatedAt, &s.ModifiedAt)
		if err != nil {
			if err == sql.ErrNoRows {
				return echo.NewHTTPError(404, "Server not found")
			}
			return err
		}

		_, err = db.Exec(`DELETE FROM servers WHERE id = ?`, s.ID)
		if err != nil {
			return err
		}

		c.JSON(http.StatusOK, s)
		return nil
	}
}

func GetGamesOfServer(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		games := make([]models.Game, 0)
		sid := c.Param("id")
		orderBy := c.QueryParam("sort")
		offset := c.QueryParam("offset")
		limit := c.QueryParam("limit")

		if orderBy == "" {
			orderBy = "id"
		}

		if offset == "" {
			offset = "0"
		}

		if limit == "" || limit > "100" {
			limit = "10"
		}

		err := db.QueryRow(`SELECT id FROM servers WHERE id = ?`, sid).Scan()
		if err != nil {
			return echo.NewHTTPError(http.StatusNotFound, "server not found")
		}

		rows, err := db.Query(
			`SELECT
				id, title, description, developer_id,
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM severs_games
			JOIN games ON games.id = severs_games.game_id
			WHERE server_id = ?
			ORDER BY ? LIMIT ? OFFSET ?`,
			sid, orderBy, limit, offset)
		if err != nil {
			return err
		}

		for rows.Next() {
			g := models.Game{}
			err = rows.Scan(
				&g.ID, &g.Title, &g.DeveloperID,
				&g.LatestTokenIssuedAt,
				&g.Status, &g.StatusChangedAt, &g.StatusMessage,
				&g.CreatedAt, &g.ModifiedAt)

			if err != nil {
				return err
			}

			games = append(games, g)
		}

		c.JSON(http.StatusOK, games)
		return nil
	}
}

func PostGameToServer(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		sid := c.Param("id")
		gid := c.FormValue("game_id")
		g := models.Game{}

		err := db.QueryRow(`SELECT id FROM servers WHERE id = ?`, sid).Scan()
		if err != nil {
			return echo.NewHTTPError(http.StatusNotFound, "server not found")
		}

		err = db.QueryRow(
			`SELECT
				id, title, description, developer_id,
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM games
			WHERE id = $1`, gid).Scan(
			&g.ID, &g.Title, &g.DeveloperID,
			&g.LatestTokenIssuedAt,
			&g.Status, &g.StatusChangedAt, &g.StatusMessage,
			&g.CreatedAt, &g.ModifiedAt)
		if err != nil {
			if err == sql.ErrNoRows {
				return echo.NewHTTPError(404, "Game not found")
			}
			return err
		}

		err = db.QueryRow(
			`INSERT INTO servers_games(server_id, game_id) VALUES(?, ?)`,
			sid, gid).Scan()
		if err != nil {
			return err
		}

		c.JSON(http.StatusOK, g)
		return nil
	}
}

func DeleteGameFromServer(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		sid := c.Param("id")
		gid := c.FormValue("game_id")
		g := models.Game{}

		err := db.QueryRow(`SELECT id FROM servers WHERE id = ?`, sid).Scan()
		if err != nil {
			return echo.NewHTTPError(http.StatusNotFound, "server not found")
		}

		err = db.QueryRow(
			`SELECT
				id, title, description, developer_id,
				latest_token_issued_at,
				status, status_changed_at, status_message,
				created_at, modified_at
			FROM games
			WHERE id = $1`, gid).Scan(
			&g.ID, &g.Title, &g.DeveloperID,
			&g.LatestTokenIssuedAt,
			&g.Status, &g.StatusChangedAt, &g.StatusMessage,
			&g.CreatedAt, &g.ModifiedAt)
		if err != nil {
			if err == sql.ErrNoRows {
				return echo.NewHTTPError(404, "Game not found")
			}
			return err
		}

		err = db.QueryRow(
			`DELETE FROM servers_games WHERE server_id = ? AND game_id = ?`,
			sid, gid).Scan()
		if err != nil {
			return err
		}

		c.JSON(http.StatusOK, g)

		return nil
	}
}
